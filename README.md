# Sappers Repo (Repositorio de Proyectos con Sapper)

**Repo para proyectos realizados con los frameworks Sapper & Svelte**

Folder | Descripción
--- | ---
EncuestApp | App para seleccionar una opción correcta.
MiApp | One page experimental. 
Lest | App de encuesta para lesbianas.
Moki | App de recursos para programar.

Repo creado por **Marco Antonio Castillo Reyes**
