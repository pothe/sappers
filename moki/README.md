# Moki App

## En esta app se han modificado los estilos de la plantilla original de Sapper

# Modificaciones

Folder | Descripción
--- | ---
static | Aquí van fotos, imágenes, ilustraciones, etc. Se agregan así src="foto.jpg"
components | Aquí van los componentes que van en la interface.
_layout.svelte | Archivo .svelte que será la plantilla de todas las páginas de la app.
guia.svelte | Archivo .svelte que se importa con una variable.


**Repo creado por Marco Antonio Castillo Reyes**
